import uuid
from typing import Dict


class User:
    def __init__(self, email):
        self.email = email
        self.__user_id = str(uuid.uuid4())

    @property
    def user_id(self):
        return self.__user_id

    def to_dict(self) -> Dict:
        return {
            'email': self.email,
            'id': self.__user_id
        }
