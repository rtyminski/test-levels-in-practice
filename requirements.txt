flask==2.2.2
flask-restful==0.3.9
kafka-python==2.0.2
pytest==7.3.1
pytest-flask==1.2.0
